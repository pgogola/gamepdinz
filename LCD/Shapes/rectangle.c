/*
 * rectangle.c
 *
 *  Created on: 23.08.2018
 *      Author: Piotr
 */

#include "rectangle.h"

#include <lcd.h>
#include <stdlib.h>

Rectangle *constructor_Rectangle(int32_t xUpperLeftCorner,
		int32_t yUpperLeftCorner, int32_t xBottomRightCorner,
		int32_t yBottomRightCorner, bool isFilledUp, uint32_t color)
{
	if (xUpperLeftCorner < LCD_X_MIN_SCREEN_COORDINATION
			|| yUpperLeftCorner < LCD_Y_MIN_SCREEN_COORDINATION
			|| xBottomRightCorner > LCD_X_MAX_SCREEN_COORDINATION
			|| yBottomRightCorner > LCD_Y_MAX_SCREEN_COORDINATION)
	{
		return NULL;
	}
	Rectangle* rectangle = (Rectangle*) malloc(sizeof(Rectangle));
	if (NULL != rectangle)
	{
		rectangle->xUpperLeftCorner_ = xUpperLeftCorner;
		rectangle->yUpperLeftCorner_ = yUpperLeftCorner;
		rectangle->xBottomRightCorner_ = xBottomRightCorner;
		rectangle->yBottomRightCorner_ = yBottomRightCorner;
		rectangle->color_ = color;
		rectangle->isFilledUp_ = isFilledUp;
		rectangle->width_ = xBottomRightCorner - xUpperLeftCorner;
		rectangle->height_ = yBottomRightCorner - yUpperLeftCorner;
	}
	return rectangle;
}

void destructor_Rectangle(Rectangle* const rectangle)
{
	if (NULL != rectangle)
	{
		free(rectangle);
	}
}

bool moveTo_Rectangle(Rectangle* const rectangle, uint32_t xUpperLeftCorner,
		uint32_t yUpperLeftCorner)
{
	uint32_t newXBottomRightCorner = rectangle->width_ + xUpperLeftCorner;
	uint32_t newYBottomRightCorner = rectangle->height_ + yUpperLeftCorner;
	if (NULL == rectangle||
	xUpperLeftCorner < LCD_X_MIN_SCREEN_COORDINATION ||
	yUpperLeftCorner < LCD_Y_MIN_SCREEN_COORDINATION ||
	newXBottomRightCorner > LCD_X_MAX_SCREEN_COORDINATION ||
	newYBottomRightCorner > LCD_Y_MAX_SCREEN_COORDINATION)
	{
		return false;
	}
	rectangle->xUpperLeftCorner_ = xUpperLeftCorner;
	rectangle->yUpperLeftCorner_ = yUpperLeftCorner;
	rectangle->xBottomRightCorner_ = newXBottomRightCorner;
	rectangle->yBottomRightCorner_ = newYBottomRightCorner;
	return true;
}

bool moveBy_Rectangle(Rectangle* const rectangle, int32_t x, int32_t y)
{
	return moveTo_Rectangle(rectangle, x + rectangle->xUpperLeftCorner_,
			y + rectangle->yUpperLeftCorner_);
}

bool draw_Rectangle(Rectangle* const rectangle)
{
	if (NULL == rectangle)
	{
		return false;
	}
	BSP_LCD_SetTextColor(rectangle->color_);
	printInformation_Rectangle(rectangle, "draw");
	if (true == rectangle->isFilledUp_)
	{
		BSP_LCD_FillRect(rectangle->xUpperLeftCorner_+4,
				rectangle->yUpperLeftCorner_+4, rectangle->width_-4,
				rectangle->height_-4);
	} else
	{
		BSP_LCD_DrawRect(rectangle->xUpperLeftCorner_,
				rectangle->yUpperLeftCorner_, rectangle->width_,
				rectangle->height_);
	}
	return true;
}


void printInformation_Rectangle(Rectangle* const rectangle, const char* description)
{
	print("%s", description);
	print(" RECTANGLE: xULeft=%d\tyULeft=%d\txBRight=%d\tyBRight=%d\r\n", rectangle->xUpperLeftCorner_,
			rectangle->yUpperLeftCorner_, rectangle->xBottomRightCorner_, rectangle->yBottomRightCorner_);
}
