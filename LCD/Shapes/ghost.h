/*
 * ghost.h
 *
 *  Created on: 16.08.2018
 *      Author: Piotr
 */

#ifndef SHAPES_GHOST_H_
#define SHAPES_GHOST_H_

#include <common.h>
#include <Shapes/pacman.h>
#include "board.h"
#include "circle.h"

typedef struct
{
	uint32_t color_;
	int32_t x_;
	int32_t y_;
	int32_t xSize_;
	int32_t ySize_;
	uint8_t targetX_;
	uint8_t targetY_;
	uint8_t lastX_;
	uint8_t lastY_;
	Circle* pack_;
	bool reachedTarget_;

	uint32_t initialX_;
	uint32_t initialY_;

	Coordinate targetPoints_[2];
	uint8_t currentTargetPoint_;
} Ghost;

float distance(int32_t currX, int32_t currY, int32_t destX, int32_t destY);

void chaseMovementAlgorithm(Ghost* const ghost, Pacman* const pacman,
		Board* const board);

void scatterMovementAlgorithm(Ghost* const ghost, Board* const board);

Ghost *constructor_Ghost(int32_t x, int32_t y, int32_t xSize, int32_t ySize,
		uint32_t color, uint8_t target1X, uint8_t target1Y, uint8_t target2X,
		uint8_t target2Y);

void destructor_Ghost(Ghost* const ghost);

bool moveTo_Ghost(Ghost* const ghost, int32_t x, int32_t y);

bool moveBy_Ghost(Ghost* const ghost, int32_t x, int32_t y);

bool draw_Ghost(Ghost* const ghost);

bool remove_Ghost(Ghost* const ghost);

void resetPosition_Ghost(Ghost* const ghost);

#endif /* SHAPES_GHOST_H_ */
