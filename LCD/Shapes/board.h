/*
 * shapes.h
 *
 *  Created on: 14.08.2018
 *      Author: Piotr
 */

#ifndef SHAPES_BOARD_H_
#define SHAPES_BOARD_H_

#include <common.h>
#include <stdint.h>
#include "score.h"

#define BOARD_SIZE_X 30
#define BOARD_SIZE_Y 30
#define BOARD_RATIO_X 8
#define BOARD_RATIO_Y 8

typedef enum
{
	EMPTY = 0,
	WALL_H = 1,
	WALL_V = 2,
	WALL_LEFT_UP_CORNER = 3,
	WALL_RIGHT_UP_CORNER = 4,
	WALL_LEFT_BOTTOM_CORNER = 5,
	WALL_RIGHT_BOTTOM_CORNER = 6,
	GHOST = 7,
	PLAYER = 8,
	FOOT_DOT = 9,
	FOOT_BIG = 10,
	TARGET_POINT = 11,
	NO_SYMBOL = 12
} BoardSymbol;

uint32_t twoDim2OneDim(uint8_t x, uint8_t y);

typedef struct
{
	uint8_t* board_;
	Score** boardScores_;
	Coordinate* coordinates_;
	uint8_t coordinatesAmount_;
	uint32_t amountOfScores_;
	uint32_t color_;
} Board;

Board *constructor_Board();
void destructor_Board(Board* const board);
bool draw_Board(Board* const board);
bool isWall(Board* const board, uint8_t x, uint8_t y);

#endif /* SHAPES_BOARD_H_ */
