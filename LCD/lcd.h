/*
 * lcd.h
 *
 *  Created on: 30.07.2018
 *      Author: Piotr
 */

#ifndef LCD_H_
#define LCD_H_

#include <stm32f429i/stm32f429i_discovery_lcd.h>
#include <Shapes/pacman.h>
#include "Shapes/board.h"
#include "Shapes/circle.h"
#include "Shapes/ghost.h"
#include "Shapes/score.h"

#endif /* LCD_H_ */
