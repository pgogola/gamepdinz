/*
 * menu.c
 *
 *  Created on: 06.10.2018
 *      Author: Piotr
 */

#include "menu.h"
#include "common.h"
#include <stdlib.h>
#include <string.h>

Menu* construct_Menu(uint8_t labelsAmount, const char** labels,
		KeyboardStatus* const keyboard)
{
	Menu* menu = (Menu*) malloc(sizeof(Menu));
	if (NULL != menu)
	{
		menu->labelsAmount_ = labelsAmount;
		menu->labels_ = (Label*) malloc(labelsAmount * sizeof(Label));
		for (uint8_t i = 0; i < menu->labelsAmount_; i++)
		{
			strcpy(menu->labels_[i].text_, labels[i]);
			menu->labels_[i].position_ = (i + 1) * 50;
			menu->labels_[i].isActive_ = false;
		}
		menu->labels_[0].isActive_ = true;
		menu->keyboard_ = keyboard;
	}
	return menu;
}

void destructor_Menu(Menu* const menu)
{
	if (NULL != menu)
	{
		free(menu);
	}
}

static volatile uint8_t optionSelected = 0;
extern volatile uint32_t menuFlag;

uint8_t run_Menu(Menu* const menu)
{
	optionSelected = 0;
	BSP_LCD_SetTextColor(LCD_COLOR_GREEN);

	for (uint8_t i = 0; i < menu->labelsAmount_; i++)
	{
		menu->labels_[i].isActive_ = false;
	}
	menu->labels_[0].isActive_ = true;
	menu->selectedLabel_ = 0;

	menu->keyboard_->button_ = NOT_DEFINED;
	__disable_irq();
	for (uint8_t i = 0; i < menu->labelsAmount_; i++)
	{
		if (false == menu->labels_[i].isActive_)
		{
			BSP_LCD_SetBackColor(LCD_COLOR_BLACK);
			BSP_LCD_DisplayStringAt(0, menu->labels_[i].position_,
					(uint8_t*) menu->labels_[i].text_, CENTER_MODE);
		} else
		{
			BSP_LCD_SetBackColor(LCD_COLOR_BLUE);
			BSP_LCD_DisplayStringAt(0, menu->labels_[i].position_,
					(uint8_t*) menu->labels_[i].text_, CENTER_MODE);
		}
	}
	__enable_irq();
	while(0 == optionSelected)
	{
		if(menuFlag > 0)
		{
			update_menu(menu);
			__disable_irq();
			menuFlag--;
			__enable_irq();
		}
	}
	return menu->selectedLabel_;
}

void update_menu(Menu* const menu)
{
	if (NOT_DEFINED != menu->keyboard_->button_)
	{
		menu->labels_[menu->selectedLabel_].isActive_ = false;
		if (TWO == menu->keyboard_->button_)
		{
			menu->selectedLabel_ = (
					menu->selectedLabel_ <= 0 ?
							menu->labelsAmount_ - 1 : menu->selectedLabel_ - 1);
		} else if (EIGHT == menu->keyboard_->button_)
		{
			menu->selectedLabel_ = (menu->selectedLabel_ + 1)
					% menu->labelsAmount_;
		} else if (FIVE == menu->keyboard_->button_)
		{
			menu->keyboard_->button_ = NOT_DEFINED;
			optionSelected = 1;

		}
		menu->labels_[menu->selectedLabel_].isActive_ = true;
	}
	menu->keyboard_->button_ = NOT_DEFINED;
	__disable_irq();
	for (uint8_t i = 0; i < menu->labelsAmount_; i++)
	{
		if (false == menu->labels_[i].isActive_)
		{
			BSP_LCD_SetBackColor(LCD_COLOR_BLACK);
			BSP_LCD_DisplayStringAt(0, menu->labels_[i].position_,
					(uint8_t*) menu->labels_[i].text_, CENTER_MODE);
		} else
		{
			BSP_LCD_SetBackColor(LCD_COLOR_BLUE);
			BSP_LCD_DisplayStringAt(0, menu->labels_[i].position_,
					(uint8_t*) menu->labels_[i].text_, CENTER_MODE);
		}
	}
	__enable_irq();
}
