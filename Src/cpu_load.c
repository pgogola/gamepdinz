/*
 * cpu_load.c
 *
 *  Created on: 15.11.2018
 *      Author: Piotr
 */

#include "cpu_load.h"
#include "stm32f4xx_hal.h"
#include "common.h"

extern TIM_HandleTypeDef htim2;
extern volatile uint32_t pacmanFlag;
extern volatile uint32_t ghostFlag;

void CPULoad_init(CPULoad * const cpuLoad)
{
	cpuLoad->cycle_ = 0;
	cpuLoad->emptyCycle_ = 0;
	cpuLoad->load_ = 0;
	CoreDebug->DEMCR |= CoreDebug_DEMCR_TRCENA_Msk;
	DWT->CTRL &= ~0x00000001;
	DWT->CTRL |= 0x00000001;
	DWT->CYCCNT = 0;
}

uint8_t CPULoad_get(const CPULoad* cpuLoad)
{
	return cpuLoad->load_;
}

static uint8_t irqStatus;
static uint32_t stopIdle;
static uint32_t working;

#define _ARRAY__ 0

static uint32_t measureNum = 0;

#if _ARRAY__
static uint32_t tim2_autoreloadValue[] =
{ 899999, 899999, 854999, 809998, 764999, 719999, 674999, 629999, 584999,
		539999, 494999, 449999, 404998, 359999, 314999, 269999, 224999, 179999,
		134999, 89999, 85499, 80999, 76499, 71999, 67499, 62999, 58499, 53999,
		49499, 44999, 40499, 35999, 31499, 26999, 22499, 17999, 13499, 8999,
		4499 };
#else
static uint32_t tim2_autoreloadValue = 29999;
#endif

static uint32_t index_autoreloadValue = 0;

void CPULoad_sleep(CPULoad * const cpuLoad)
{
	working = DWT->CYCCNT;
	irqStatus = __get_PRIMASK();
	__disable_irq();
	cpuLoad->cycle_ += working;
	DWT->CYCCNT = 0;
	__WFI();
	stopIdle = DWT->CYCCNT;
	cpuLoad->cycle_ += stopIdle;
	cpuLoad->emptyCycle_ += stopIdle;
#if _ARRAY__
	if (cpuLoad->cycle_ >= HAL_RCC_GetHCLKFreq() && index_autoreloadValue < 39)
#else
	if (cpuLoad->cycle_ >= HAL_RCC_GetHCLKFreq() && index_autoreloadValue < 39)
#endif
	{
		measureNum++;
//		printf("%lu\r\n",
//				(uint32_t)(100 - ((float) cpuLoad->emptyCycle_ / cpuLoad->cycle_) * 100));
#if _ARRAY__
		printf("%lu %lu %lu\r\n", cpuLoad->emptyCycle_, cpuLoad->cycle_,
				tim2_autoreloadValue[index_autoreloadValue]);
#else
		printf("%lu %lu %lu\r\n", cpuLoad->emptyCycle_, cpuLoad->cycle_,
				tim2_autoreloadValue);
#endif
		cpuLoad->emptyCycle_ = 0;
		cpuLoad->cycle_ = 0;
		if (measureNum >= 10)
		{
#if _ARRAY__
			index_autoreloadValue++;
			__HAL_TIM_SET_AUTORELOAD(&htim2,
					tim2_autoreloadValue[index_autoreloadValue]);
			__HAL_TIM_SET_PRESCALER(&htim2, 0);
			__HAL_TIM_SET_COUNTER(&htim2, 0);
#else
			tim2_autoreloadValue -= 5000;
			__HAL_TIM_SET_AUTORELOAD(&htim2,
					tim2_autoreloadValue);
			__HAL_TIM_SET_PRESCALER(&htim2, 0);
			__HAL_TIM_SET_COUNTER(&htim2, 0);
//			printf("T %lu\r\n", tim2_autoreloadValue);
#endif
			measureNum = 0;
			pacmanFlag = 0;
			ghostFlag = 0;
		}
	}
	DWT->CYCCNT = 0;
	if (!irqStatus)
	{
		__enable_irq();
	}
}
