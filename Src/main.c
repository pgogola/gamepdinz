/**
 ******************************************************************************
 * @file           : main.c
 * @brief          : Main program body
 ******************************************************************************
 ** This notice applies to any and all portions of this file
 * that are not between comment pairs USER CODE BEGIN and
 * USER CODE END. Other portions of this file, whether
 * inserted by the user or by software development tools
 * are owned by their respective copyright owners.
 *
 * COPYRIGHT(c) 2018 STMicroelectronics
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *   3. Neither the name of STMicroelectronics nor the names of its contributors
 *      may be used to endorse or promote products derived from this software
 *      without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************
 */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f4xx_hal.h"
#include "dma.h"
#include "dma2d.h"
#include "ltdc.h"
#include "spi.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"
#include "fmc.h"

/* USER CODE BEGIN Includes */
#include <stdint.h>
#include <time.h>
#include <stdlib.h>
#include "game.h"
#include "menu.h"
#include "cpu_load.h"
/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/

KeyboardStatus* keyboard;
Game* game;
Menu* menu;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */

volatile uint32_t measureStart = 0;
volatile uint32_t measureFinish = 0;

volatile uint32_t pacmanFlag = 0;
volatile uint32_t ghostFlag = 0;
volatile uint32_t menuFlag = 0;

uint8_t isGame = 0;

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	if (TIM1 == htim->Instance)
	{
		setColumn(keyboard);
	} else if (TIM2 == htim->Instance)
	{
		ghostFlag++;
	} else if (TIM3 == htim->Instance)
	{
		if (CHASE_MOVEMENT == game->ghostMovementType_)
		{
			game->ghostMovementType_ = SCATTER_MOVEMENT;
			__HAL_TIM_SET_AUTORELOAD(&htim3, SCATTER_MODE_TIME_AUTORELOAD);
			__HAL_TIM_SET_PRESCALER(&htim3, SCATTER_MODE_TIME_PRESCALER);
			__HAL_TIM_SET_COUNTER(&htim3, 0);
			printf("SCATTER_MOVEMENT\r\n");
		} else
		{
			game->ghostMovementType_ = CHASE_MOVEMENT;
			__HAL_TIM_SET_AUTORELOAD(&htim3, CHASE_MODE_TIME_AUTORELOAD);
			__HAL_TIM_SET_PRESCALER(&htim3, CHASE_MODE_TIME_PRESCALER);
			__HAL_TIM_SET_COUNTER(&htim3, 0);
			printf("CHASE_MOVEMENT\r\n");
		}
	}
}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	if (BUTTON_ROW_1_Pin == GPIO_Pin)
	{
		keyboard->button_ = keyboard->setColumn_ + 1;
	} else if (BUTTON_ROW_2_Pin == GPIO_Pin)
	{
		keyboard->button_ = keyboard->setColumn_ + 4;
	} else if (BUTTON_ROW_3_Pin == GPIO_Pin)
	{
		keyboard->button_ = keyboard->setColumn_ + 7;
	} else if (BUTTON_ROW_4_Pin == GPIO_Pin)
	{
		keyboard->button_ = keyboard->setColumn_ + 10;
	}
	if (1 == isGame)
	{
		pacmanFlag++;
	} else if (2 == isGame)
	{
		menuFlag++;
	}
}

static void initialize()
{
	BSP_LCD_LayerDefaultInit(LCD_BACKGROUND_LAYER,
	LCD_FRAME_BUFFER + BUFFER_OFFSET);
	BSP_LCD_SelectLayer(LCD_BACKGROUND_LAYER);
	BSP_LCD_DisplayOn();
	BSP_LCD_Clear(LCD_COLOR_BLACK);
	BSP_LCD_LayerDefaultInit(LCD_FOREGROUND_LAYER, LCD_FRAME_BUFFER);
	BSP_LCD_SelectLayer(LCD_FOREGROUND_LAYER);
	BSP_LCD_DisplayOn();
	BSP_LCD_Clear(LCD_COLOR_BLACK);
	BSP_LCD_SelectLayer(LCD_FOREGROUND_LAYER);

	BSP_LCD_SetBackColor(LCD_COLOR_BLACK);

	const char* menu1labels[] =
	{ "Start", "Stop" };

	srand(time(NULL));
	keyboard = construct_Keyboard();
	menu = construct_Menu(2, menu1labels, keyboard);
	game = construct_Game(5, keyboard);

	HAL_GPIO_WritePin(BUTTON_COL_1_GPIO_Port, BUTTON_COL_1_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(BUTTON_COL_2_GPIO_Port, BUTTON_COL_2_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(BUTTON_COL_3_GPIO_Port, BUTTON_COL_3_Pin, GPIO_PIN_RESET);
	menuFlag = 0;
	pacmanFlag = 0;
	ghostFlag = 0;
}

static void deinitialize()
{
	destructor_Game(game);
	destructor_Keyboard(keyboard);
	destructor_Menu(menu);
	keyboard = NULL;
	game = NULL;
	menu = NULL;
}

/* USER CODE END 0 */

/**
 * @brief  The application entry point.
 *
 * @retval None
 */
int main(void)
{
	/* USER CODE BEGIN 1 */

	/* USER CODE END 1 */

	/* MCU Configuration----------------------------------------------------------*/

	/* Reset of all peripherals, Initializes the Flash interface and the Systick. */
	HAL_Init();

	/* USER CODE BEGIN Init */

	/* USER CODE END Init */

	/* Configure the system clock */
	SystemClock_Config();

	/* USER CODE BEGIN SysInit */

	/* USER CODE END SysInit */

	/* Initialize all configured peripherals */
	MX_GPIO_Init();
	MX_DMA_Init();
	MX_USART1_UART_Init();
	MX_DMA2D_Init();
	MX_FMC_Init();
	MX_LTDC_Init();
	MX_SPI5_Init();
	MX_TIM1_Init();
	MX_TIM2_Init();
	MX_TIM3_Init();
	/* USER CODE BEGIN 2 */
	BSP_LCD_Init();

	/* USER CODE END 2 */

	/* Infinite loop */
	/* USER CODE BEGIN WHILE */

	CoreDebug->DEMCR |= CoreDebug_DEMCR_TRCENA_Msk;
	DWT->CTRL &= ~0x00000001;
	DWT->CTRL |= 0x00000001;

	enum GameStatus gameStatus;
	uint8_t menuOption = 0;
	const char* infoGameWon[] =
	{ "GAME WON", "Press '5'", "to reset" };

	const char* infoGameLost[] =
	{ "GAME LOST", "Press '5'", "to reset" };
	HAL_TIM_Base_Start_IT(&htim1);
	while (1)
	{
		initialize();
		isGame = 2;
		__HAL_TIM_SET_AUTORELOAD(&htim1, KEYBOARD_REFRESH_TIME_MENU_AUTORELOAD);
		__HAL_TIM_SET_PRESCALER(&htim1, KEYBOARD_REFRESH_TIME_MENU_PRESCALER);
		__HAL_TIM_SET_COUNTER(&htim1, 0);
		menuOption = run_Menu(menu);
		if (0 == menuOption)
		{
			__HAL_TIM_SET_AUTORELOAD(&htim1,
					KEYBOARD_REFRESH_TIME_GAME_AUTORELOAD);
			__HAL_TIM_SET_PRESCALER(&htim1,
					KEYBOARD_REFRESH_TIME_GAME_PRESCALER);
			__HAL_TIM_SET_COUNTER(&htim1, 0);
			isGame = 1;
			initialize_Game(game);
			gameStatus = play(game);

			isGame = 0;
			if (GAME_LOST == gameStatus)
			{
				gameInformationLines(infoGameLost, 3);
			} else if (GAME_WON == gameStatus)
			{
				gameInformationLines(infoGameWon, 3);
			}
			__HAL_TIM_SET_AUTORELOAD(&htim1,
					KEYBOARD_REFRESH_TIME_MENU_AUTORELOAD);
			__HAL_TIM_SET_PRESCALER(&htim1,
					KEYBOARD_REFRESH_TIME_MENU_PRESCALER);
			__HAL_TIM_SET_COUNTER(&htim1, 0);
			while (FIVE != keyboard->button_)
			{
			}
			keyboard->button_ = NOT_DEFINED;
			game->gameStatus_ = GAME_FINISHED;
		}
		deinitialize();
		/* USER CODE END WHILE */

		/* USER CODE BEGIN 3 */
	}
	/* USER CODE END 3 */

}

/**
 * @brief System Clock Configuration
 * @retval None
 */
void SystemClock_Config(void)
{

	RCC_OscInitTypeDef RCC_OscInitStruct;
	RCC_ClkInitTypeDef RCC_ClkInitStruct;
	RCC_PeriphCLKInitTypeDef PeriphClkInitStruct;

	/**Configure the main internal regulator output voltage
	 */
	__HAL_RCC_PWR_CLK_ENABLE()
	;

	__HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

	/**Initializes the CPU, AHB and APB busses clocks
	 */
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
	RCC_OscInitStruct.HSIState = RCC_HSI_ON;
	RCC_OscInitStruct.HSICalibrationValue = 16;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
	RCC_OscInitStruct.PLL.PLLM = 8;
	RCC_OscInitStruct.PLL.PLLN = 180;
	RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
	RCC_OscInitStruct.PLL.PLLQ = 4;
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
	{
		_Error_Handler(__FILE__, __LINE__);
	}

	/**Activate the Over-Drive mode
	 */
	if (HAL_PWREx_EnableOverDrive() != HAL_OK)
	{
		_Error_Handler(__FILE__, __LINE__);
	}

	/**Initializes the CPU, AHB and APB busses clocks
	 */
	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK
			| RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK)
	{
		_Error_Handler(__FILE__, __LINE__);
	}

	PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_LTDC;
	PeriphClkInitStruct.PLLSAI.PLLSAIN = 216;
	PeriphClkInitStruct.PLLSAI.PLLSAIR = 2;
	PeriphClkInitStruct.PLLSAIDivR = RCC_PLLSAIDIVR_2;
	if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK)
	{
		_Error_Handler(__FILE__, __LINE__);
	}

	/**Configure the Systick interrupt time
	 */
	HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq() / 1000);

	/**Configure the Systick
	 */
	HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

	/* SysTick_IRQn interrupt configuration */
	HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
 * @brief  This function is executed in case of error occurrence.
 * @param  file: The file name as string.
 * @param  line: The line in file as a number.
 * @retval None
 */
void _Error_Handler(char *file, int line)
{
	/* USER CODE BEGIN Error_Handler_Debug */
	/* User can add his own implementation to report the HAL error return state */
	while (1)
	{
	}
	/* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
 * @brief  Reports the name of the source file and the source line number
 *         where the assert_param error has occurred.
 * @param  file: pointer to the source file name
 * @param  line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t* file, uint32_t line)
{
	/* USER CODE BEGIN 6 */
	/* User can add his own implementation to report the file name and line number,
	 tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
	/* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/**
 * @}
 */

/**
 * @}
 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
