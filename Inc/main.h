/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2018 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H__
#define __MAIN_H__

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private define ------------------------------------------------------------*/
#define TIM1_PSC 1999
#define TIM1_ARR 8999
#define TIM1_RCR 0
#define TIM2_PSC 999
#define TIM2_ARR 9999
#define TIM2_RCR 0
#define TIM3_PSC 59999
#define TIM3_ARR 29999
#define TIM3_RCR 0

#define BUTTON_ROW_1_Pin GPIO_PIN_3
#define BUTTON_ROW_1_GPIO_Port GPIOE
#define BUTTON_ROW_1_EXTI_IRQn EXTI3_IRQn
#define BUTTON_ROW_2_Pin GPIO_PIN_4
#define BUTTON_ROW_2_GPIO_Port GPIOE
#define BUTTON_ROW_2_EXTI_IRQn EXTI4_IRQn
#define BUTTON_ROW_3_Pin GPIO_PIN_5
#define BUTTON_ROW_3_GPIO_Port GPIOE
#define BUTTON_ROW_3_EXTI_IRQn EXTI9_5_IRQn
#define BUTTON_ROW_4_Pin GPIO_PIN_6
#define BUTTON_ROW_4_GPIO_Port GPIOE
#define BUTTON_ROW_4_EXTI_IRQn EXTI9_5_IRQn
#define BUTTON_COL_1_Pin GPIO_PIN_13
#define BUTTON_COL_1_GPIO_Port GPIOC
#define BUTTON_COL_2_Pin GPIO_PIN_14
#define BUTTON_COL_2_GPIO_Port GPIOC
#define BUTTON_COL_3_Pin GPIO_PIN_15
#define BUTTON_COL_3_GPIO_Port GPIOC
#define BUTTON_OK_Pin GPIO_PIN_0
#define BUTTON_OK_GPIO_Port GPIOA
#define BUTTON_OK_EXTI_IRQn EXTI0_IRQn

/* ########################## Assert Selection ############################## */
/**
  * @brief Uncomment the line below to expanse the "assert_param" macro in the 
  *        HAL drivers code
  */
/* #define USE_FULL_ASSERT    1U */

/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
 extern "C" {
#endif
void _Error_Handler(char *, int);

#define Error_Handler() _Error_Handler(__FILE__, __LINE__)
#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H__ */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
