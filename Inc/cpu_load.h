/*
 * cpu_load.h
 *
 *  Created on: 15.11.2018
 *      Author: Piotr
 */

#ifndef CPU_LOAD_H_
#define CPU_LOAD_H_

#include <stdint.h>

typedef struct{
	uint32_t cycle_;
	uint32_t emptyCycle_;
	uint8_t load_;
}CPULoad;

void CPULoad_init(CPULoad *const cpuLoad);
uint8_t CPULoad_get(const CPULoad* cpuLoad);
void CPULoad_sleep(CPULoad * const cpuLoad);

#endif /* CPU_LOAD_H_ */
